<?php

include 'config.php';

function change_key($array, $key)
{
    $result = [];
    $work = array_map(function ($v) use ($key, &$result) {
        $result[$v[$key]] = $v['id'];
    }, $array);
    return $result;

}

$result = change_key($array, 'name');
print_r($result);
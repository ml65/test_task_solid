<?php

include 'config.php';

/**
 * @param $array
 * @param $key
 * @return void
 */
function sort_key(&$array , $key) {
    uasort($array, function ($a, $b) use ($key) {
        if($a[$key] == $b[$key]) return 0;
        return ( $a[$key] < $b[$key]) ? -1 : 1;
    });
}


sort_key($array, 'name');

print_r($array);

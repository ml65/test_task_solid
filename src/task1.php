<?php

include "config.php";

/**
 * @param $array
 * @param $double - массив дублей
 * @return false
 */
function step1($array, &$double)
{
    // выделяем ключи
    $keys = array_column($array, 'id');
    // создаем ассоциативный массив и снова преобразуем в обычный
    $result = array_values(array_combine($keys, $array));
    // ищем дубли
    $a = array_map(function ($v) {
        return json_encode($v);
    }, $array);
    $b = array_map(function ($v) {
        return json_encode($v);
    }, $result);
    $double = array_map(function ($v) {
        return json_decode($v);
    }, array_diff($a, $b));
    return $result;
}

$double = [];

$result = step1($array, $double);
echo "Результат: ";
print_r($result);
echo "\nДубли: ";
print_r($double);

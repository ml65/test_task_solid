<?php

interface SomeObjectInterface
{
    public function getObjectName();
}

class SomeObject implements SomeObjectInterface {
    protected $name;

    public function __construct(string $name) { }

    public function getObjectName() { }
}

class SomeObjectsHandler
{
    public function __construct() { }

    /**
     * @param array $objects
     * @return array
     *
     * Если нужна информация по фиксированному списку объектов, предусмотреть еще один паараметр с массивом имен объектов
     * и вносить в массив handlers только объекты удовлетворяющие массиву.
     */
    public function handleObjects(array $objects): array {
        $handlers = [];
        foreach ($objects as $object) {
            $name = $object->getObjectName();
            $handlers[] = 'handle_' . $name;
        }

        return $handlers;
    }
}

$objects = [
    new SomeObject('object_1'),
    new SomeObject('object_2')
];

$soh = new SomeObjectsHandler();
$soh->handleObjects($objects);
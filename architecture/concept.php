<?php
/*
 * Config::get - метод фасада получения данных конфигуации
 *
 */
abstract class CreatorStorage
{
    abstract public function factoryMethod():StorageInterface;

    public function getSecretKey(): string
    {
        $storage = $this->factoryMethod();
        return $storage->getSecretKey();

    }

}

class FileCreatorStorage extends CreatorStorage
{
    public function factoryMethod():StorageInterface
    {
        // TODO: Implement factoryMethod() method.
        return new FileStorage();
    }
}

class DbCreatorStorage extends CreatorStorage
{
    public function factoryMethod():StorageInterface
    {
        // TODO: Implement factoryMethod() method.
        return new DbStorage();
    }
}

interface StorageInterface
{
    public function getSecretKey(): string;

}

class FileStorage implements StorageInterface
{
    public function getSecretKey(): string
    {
        return file_get_contents(Config::get('path.secret.key'));
    }
}

class DbStorage implements StorageInterface
{
    public function getSecretKey(): string
    {
        // получение данных из таблицы DB
        $result = '';

        return $result;
    }
}


class Concept {
    private $client;
    private $storage;

    public function __construct(CreatorStorage $storage) {
        $this->storage = $storage;
        $this->client = new \GuzzleHttp\Client();
    }

    public function getUserData() {
        $params = [
            'auth' => ['user', 'pass'],
            'token' => $this->getSecretKey()
        ];

        $request = new \Request('GET', 'https://api.method', $params);
        $promise = $this->client->sendAsync($request)->then(function ($response) {
            $result = $response->getBody();
        });

        $promise->wait();
    }
}

$get = new Concept(new FileCreatorStorage());
$result = $get->getUserData();



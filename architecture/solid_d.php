<?php

interface XMLHttpServiceInterface {
    public function request(string $url, string $method, array $options=[]);
}

class XMLHttpService extends XMLHTTPRequestService implements XMLHttpServiceInterface {
    public function request(string $url, string $method, array $options = [])
    {
        // TODO: Implement request() method.
    }
}

class Http {

    private $service;

    public function __construct(XMLHttpServiceInterface $xmlHttpService) {
        $this->service = $xmlHttpService;
    }

    public function get(string $url, array $options) {
        $this->service->request($url, 'GET', $options);
    }

    public function post(string $url) {
        $this->service->request($url, 'GET');
    }
}
